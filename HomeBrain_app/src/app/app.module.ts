import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { DetallePage } from '../pages/detalle/detalle';
import { EditarPage } from '../pages/editar/editar';
import { EliminarPage } from '../pages/eliminar/eliminar';
import { TabsPage } from '../pages/tabs/tabs';
import { Tabs2Page } from '../pages/tabs2/tabs2';
import { InventarioPage } from '../pages/inventario/inventario';
import { LoginPage } from '../pages/login/login';
import { BienvenidaPage } from '../pages/bienvenida/bienvenida';
import { RegistrarsePage } from '../pages/registrarse/registrarse';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    InventarioPage,
    DetallePage,
    EditarPage,
    EliminarPage,
    LoginPage,
    BienvenidaPage,
    RegistrarsePage,
    TabsPage,
    Tabs2Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage, 
    HomePage,
    InventarioPage,
    DetallePage,
    EditarPage,
    EliminarPage,
    LoginPage,
    BienvenidaPage,
    RegistrarsePage,
    TabsPage,
    Tabs2Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
