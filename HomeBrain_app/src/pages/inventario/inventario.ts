import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetallePage } from '../detalle/detalle';

/**
 * Generated class for the InventarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inventario',
  templateUrl: 'inventario.html',
})
export class InventarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  onDetalle(){
    this.navCtrl.push(DetallePage, {
      item: 'primero'
    });
    
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad InventarioPage');
  }

}
